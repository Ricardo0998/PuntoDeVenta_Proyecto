package controlador;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import modelo.ModeloCliente;
import modelo.ModeloCompras;
import modelo.ModeloPrincipal;
import modelo.ModeloProducto;
import modelo.ModeloProveedores;
import vista.Clientes;
import vista.Compras;
import vista.Principal;
import vista.Productos;
import vista.Proveedores;

public class ControladorPrincipal implements ActionListener{

    //Declaracion de la vista y el modelo
    private Principal vista;
    private ModeloPrincipal modelo;

    //Constructor que inicializa la vista y el modelo de Principal
    public ControladorPrincipal(Principal vista, ModeloPrincipal modelo) {
        this.vista = vista;
        this.modelo = modelo;
    }
    
    //Metodo que hace visible a la ventana y darle propiedades
    public void iniciar(){
        vista.setTitle("Vista Principal");
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
        asignarControl();
    }
    
    //Metodo que hace que el controlador pueda escuchar las acciones de la vista de Principal
    private void asignarControl(){
        vista.getJb_proveedores().addActionListener(this);
        vista.getJb_cliente().addActionListener(this);
        vista.getJb_compras().addActionListener(this);
        vista.getJb_productos().addActionListener(this);
        vista.getJb_ventas().addActionListener(this);
    }
    
    //Metodo sobreescrito que detectara los eventos y asignara el evento que realizara cada boton
    @Override
    public void actionPerformed(ActionEvent ae) {
        if(ae.getSource() == vista.getJb_proveedores()){
            EventQueue.invokeLater(new Runnable(){
                @Override
                public void run(){
                    new ControladorProveedores(new Proveedores(), new ModeloProveedores()).iniciar();
                }
            });
            vista.dispose();
        }else if(ae.getSource() == vista.getJb_cliente()){
            EventQueue.invokeLater(new Runnable(){
                @Override
                public void run(){
                    new ControladorCliente(new Clientes(), new ModeloCliente()).iniciar();
                }
            });
            vista.dispose();
        }else if(ae.getSource() == vista.getJb_compras()){
            EventQueue.invokeLater(new Runnable(){
                @Override
                public void run(){
                    new ControladorCompras(new Compras(), new ModeloCompras()).iniciar();
                }
            });
            vista.dispose();
        }else if(ae.getSource() == vista.getJb_productos()){
            EventQueue.invokeLater(new Runnable(){
                @Override
                public void run(){
                    new ControladorProducto(new Productos(), new ModeloProducto()).iniciar();
                }
            });
            vista.dispose();
        }else if(ae.getSource() == vista.getJb_ventas()){
            System.out.println("Ventas");
        }
    }
    
}
