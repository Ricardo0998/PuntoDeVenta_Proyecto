package puntodeventa;

import controlador.ControladorPrincipal;
import java.awt.EventQueue;
import modelo.ModeloPrincipal;
import vista.Principal;

public class PuntoDeVenta {

    public static void main(String[] args) {
        //Inicio de la app, corriendo la vista principal
        EventQueue.invokeLater(new Runnable(){
            @Override
            public void run(){
                new ControladorPrincipal(new Principal(), new ModeloPrincipal()).iniciar();
            }
        });
        
    }
    
}
